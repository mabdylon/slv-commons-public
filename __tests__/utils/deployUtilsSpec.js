const AWS = require('aws-sdk');
const deployUtils = require('../../src/utils/deployUtils');

const mockPutObject = jest.fn();
AWS.S3 = jest.fn().mockImplementation( ()=> {
    return {
      putObject (params, cb) {
        mockPutObject(params, cb);
      }
    };
});


describe(`deployUtils`, () => {

    describe(`uploadDir`, () => {

        test(`should walk dir and upload it to s3`, () => {
            const directory = "mocks/mockDist";
            const bucketName = "randomBucket"
            deployUtils.uploadDir(directory, bucketName);
            expect(mockPutObject).toHaveBeenCalledWith(
                expect.objectContaining({
                    'Key':'mockZip.zip',
                    'Bucket': bucketName
                }),
                expect.any(Function)
            );
        });

    });

});
