const validatorUtils = require(`../../src/utils/validatorUtils`);

describe(`validator`, () => {

    describe(`validateEmail`, () => {

        test(`should return true if email have the correct format`, () => {
            const email = "john.doe@mail.com"
            const isEmail = validatorUtils.isEmail(email);
            expect(isEmail).toBeTruthy();

        });

        test(`should return false if email has no '@'`, () => {
            const email = "john.doe"
            const isEmail = validatorUtils.isEmail(email);
            expect(isEmail).toBeFalsy();
        });

        test(`should return false if email has dot`, () => {
            const email = "john"
            const isEmail = validatorUtils.isEmail(email);
            expect(isEmail).toBeFalsy();
        });

    });

    describe(`validateUrl`, () => {
        
        test(`should return true if url is valid`, () => {
            const url = "www.seloger.com/utils";
            const isUrl = validatorUtils.isUrl(url);
            expect(isUrl).toBeTruthy();
        });

        test(`should return false if url has no dot`, () => {
            const url = "lfdsmflkd";
            const isUrl = validatorUtils.isUrl(url);
            expect(isUrl).toBeFalsy();
        });

    });

    describe(`isUrlSupported`, () => {

        test(`should return false if url is not seloger, pap or leboncoin`, () => {
            const url = "www.toto.com";
            const isUrlSupported = validatorUtils.isUrlSupported(url);
            expect(isUrlSupported).toBeFalsy();
        });
        
        test(`should return true if url is seLoger`, () => {
            const url = "www.seloger.com/";
            const isUrlSupported = validatorUtils.isUrlSupported(url);
            expect(isUrlSupported).toBeTruthy();
        });

        test(`should return true if url is pap`, () => {
            const url = "www.pap.com/";
            const isUrlSupported = validatorUtils.isUrlSupported(url);
            expect(isUrlSupported).toBeTruthy();
        });

        test(`should return true if url is leboncoin`, () => {
            const url = "www.leboncoin.fr/";
            const isUrlSupported = validatorUtils.isUrlSupported(url);
            expect(isUrlSupported).toBeTruthy();
        });

    });

    describe(`validate`, () => {

        test(`should return true if mobile format is correct with 06`, () => {
            const mobile = `0689887712`;
            const isMobile = validatorUtils.isMobile(mobile);
            expect(isMobile).toBeTruthy();
        });

        test(`should return true if mobile format is correct with +336`, () => {
            const mobile = `+33689887712`;
            const isMobile = validatorUtils.isMobile(mobile);
            expect(isMobile).toBeTruthy();
        });

        test(`should return false if mobile format is not correct with 01`, () => {
            const mobile = `0189887712`;
            const isMobile = validatorUtils.isMobile(mobile);
            expect(isMobile).toBeFalsy();
        });

        test(`should return false if mobile format is not correct with +331`, () => {
            const mobile = `+33189887712`;
            const isMobile = validatorUtils.isMobile(mobile);
            expect(isMobile).toBeFalsy();
        });

        test(`should return false if mobile format is not correct with nimportequoi`, () => {
            const mobile = `+nimportequoi`;
            const isMobile = validatorUtils.isMobile(mobile);
            expect(isMobile).toBeFalsy();
        });

    });


});