const LoggerService = require(`../services/loggerService`);
const logger = new LoggerService(this);
const AWS = require('aws-sdk');

module.exports = class ApiKeys {

    static getSecrets() {
        return ApiKeys.apiKeys;
    }

    static async fetchAwsSecrets() {
        const region = "us-east-2",
            secretName = "ApiKeys";

        var client = new AWS.SecretsManager({
            region: region
        });

        let secretPromise = new Promise((resolve, reject) => {

            client.getSecretValue({ SecretId: secretName }, function (err, data) {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                else {
                    resolve(data.SecretString);
                }
            });

        });

        try {
            const resultString = await secretPromise;
            ApiKeys.apiKeys = JSON.parse(resultString);
        } catch (exception) {
            logger.error(`Unable to retrieves / parse APIkeys ..`);
            logger.error(exception);
        }

        return ApiKeys.apiKeys;
    }

};