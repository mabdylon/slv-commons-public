const AWS = require('aws-sdk');
const path = require('path');
const fs = require('fs');
 
module.exports = class deployUtils {

    static uploadDir(s3Path, bucketName) {
        let S3 = new AWS.S3();
        deployUtils.walkSync(s3Path, function (filePath, stat) {
            let bucketPath = filePath.substring(s3Path.length + 1);
            let metaData = deployUtils.getContentTypeByFile(filePath);
            let params = { Bucket: bucketName, Key: bucketPath, Body: fs.readFileSync(filePath), ContentType: metaData};
            S3.putObject(params, function (err, data) {
                if (err) {
                    console.log(err)
                } else {
                    console.log('Successfully uploaded ' + bucketPath + ' to ' + bucketName);
                }
            });
        })
    }

    static walkSync(currentDirPath, callback) {
        fs.readdirSync(currentDirPath).forEach(function (name) {
            var filePath = path.join(currentDirPath, name);
            var stat = fs.statSync(filePath);
            if (stat.isFile()) {
                callback(filePath, stat);
            } else if (stat.isDirectory()) {
                deployUtils.walkSync(filePath, callback);
            }
        });
    }

    static getContentTypeByFile(fileName) {
        var rc = 'application/octet-stream';
        var fn = fileName.toLowerCase();
      
        if (fn.indexOf('.html') >= 0) rc = 'text/html';
        else if (fn.indexOf('.css') >= 0) rc = 'text/css';
        else if (fn.indexOf('.json') >= 0) rc = 'application/json';
        else if (fn.indexOf('.js') >= 0) rc = 'application/x-javascript';
        else if (fn.indexOf('.png') >= 0) rc = 'image/png';
        else if (fn.indexOf('.jpg') >= 0) rc = 'image/jpg';
        else if (fn.indexOf('.zip') >= 0) rc = 'application/zip';
        return rc;
      }

}