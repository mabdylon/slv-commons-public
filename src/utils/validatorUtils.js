module.exports = class ValidatorUtils {

    static isEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
    
    static isUrl(url) {
        var reUrl = /^[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$/;
        return reUrl.test(url);
    }

    static isUrlSupported(url) {
        var isSeLoger = url.includes('www.seloger.com/');
        var isPap = url.includes('www.pap.com/');
        var isLeBonCoin = url.includes('www.leboncoin.fr/');
        return (isSeLoger || isPap || isLeBonCoin);
    }
    
    static isMobile(mobile) {
        var reMobile = /^((\+)33|0)[6-7](\d{2}){4}$/;
        return reMobile.test(mobile);
    }

}