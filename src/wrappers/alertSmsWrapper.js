const LoggerService = require(`../services/loggerService`);
const ApiKeys = require(`../conf/apiKeys`);

const urlencode = require('urlencode');
const http = require('https');
const querystring = require('querystring');


module.exports = class AlertSmsWrapper {

    static getBaseSmsData() {
        return {
            'accessToken': ApiKeys.getSecrets().smsmode.key,
            'emetteur': 'SLVraiment',
            'stop': '1'
        }
    }

    static getOptions(postData) {
        return {
            hostname: 'api.smsmode.com',
            port: 443,
            path: '/http/1.6/sendSMS.do',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=ISO-8859-15',
                'Content-Length': Buffer.byteLength(postData)
            }
        }
    }

    constructor() {
        this.logger = new LoggerService(this);
    }

    async send(data) {
        this.logger.log(`Sending sms to ${data.mobile} ...`)
        let postData = AlertSmsWrapper.getBaseSmsData();
        postData.numero = data.mobile;
        postData = querystring.stringify(postData);
        postData = postData + "&message=" + urlencode(data.message, 'ISO-8859-15');

        const options = AlertSmsWrapper.getOptions(postData);
        const response = await this.postRequest(postData, options);
        return response;
    }



    postRequest(postData, options) {
        const promise = new Promise((resolve, reject) => {

            let req = http.request(options, (res) => {
                this.logger.log(`got response with status ${res.statusCode}`);

                let data = ``;
                res.on('data', chunk => {
                    data += chunk;
                });
                res.on('end', () => {
                    this.logger.log(`Got a response with data : ${data}`);
                    resolve(data);
                });

                
            }).on('error', (e) => {
                this.logger.error(e);
                reject(e);
            });

            req.write(postData);
            req.end();

        });

        return promise;
    }

}