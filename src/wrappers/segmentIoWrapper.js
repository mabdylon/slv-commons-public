const ApiKeys = require(`../conf/apiKeys`);
const AWS = require('aws-sdk');
const Analytics = require('analytics-node');
const LoggerService = require(`../services/loggerService`);

module.exports = class SegmentIoWrapper {


    static getAnalytics() {
        if (SegmentIoWrapper.analytics) {
            return SegmentIoWrapper;
        }
        SegmentIoWrapper.logger = new LoggerService(this);
        SegmentIoWrapper.logger.log(`Getting secret apiKeys for segmentIo ...`);
        const segmentIoKey = ApiKeys.getSecrets().segmentio.key;
        SegmentIoWrapper.analytics = new Analytics(segmentIoKey);
        return SegmentIoWrapper;
    }

    static track(options) {
        // {
        //     userId: this.pipeline.user.uuid,
        //     event: 'SLV-ADS-NEW',
        //     properties: {
        //         queryUrl: this.pipeline.query.url,
        //         adsUrl: payload,
        //         adsId: key,
        //         userStatus: this.pipeline.user.status,
        //         email: this.pipeline.user.email,
        //         phone: this.pipeline.user.mobile,
        //     }
        // }
        SegmentIoWrapper.analytics.track(options);
        SegmentIoWrapper.analytics.page({
            userId: options.userId,
            title: `${options.event}`,
            url: `${options.properties.payload}`,
            path: `${options.properties.adsUrl}`,
            referrer: `${options.properties.queryUrl}`
          });
        // const params = {
        //     Data: JSON.stringify(options),
        //     PartitionKey: options.event,
        //     StreamName: 'prod-slv-scrapper',
        // };
        // SegmentIoWrapper.kinesis.putRecord(params, function (err, data) {
        //     if (err) {
        //         SegmentIoWrapper.logger.error(err, err.stack);
        //     }
        //     else {

        //     }
        // });
    }

    static identify(options) {
        SegmentIoWrapper.analytics.identify(options);
    }


}