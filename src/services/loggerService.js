const fs = require('fs');

module.exports = class LoggerService {

    constructor(caller) {
        this.caller = caller;
        this.fullFileName = `./data/logger.txt`;
    }

    getDateLog() {
        return `\x1b[94m${new Date().toISOString().substring(0, 19)}\x1b[0m - `;
    }

    getConstructorLog() {
        return `\x1b[95m${this.caller.constructor.name}\x1b[0m : `;
    }

    getDateConstructorLog() {
        return this.getDateLog().concat(this.getConstructorLog());
    }

    log(message) {
        console.log(
            this.getDateConstructorLog() + `\x1b[92m${message}\x1b[0m`
        );
    }

    error(exception) {
        const dataLog = this.getDateLog();
        const constructorLog = this.getConstructorLog();
        console.error(
            this.getDateConstructorLog() + `\x1b[91m%s\x1b[0m`, `${exception}`
        );
        console.error(`\x1b[35m${exception.stack}\x1b[0m`);
    }

    writeFile(message) {
        fs.writeFileSync(this.fullFileName + new Date().toISOString().substring(0, 19), message, 'utf-8');
    }

}